const User = require('../models/user');
const Geo = require('geo-nearby');
/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.postSearchFriend = function (req, res, next) {
    User.find({email: req.body.email}, function (err, user) {
        res.send({success: true, data: user});
    });
};

function uniqBy(a, key) {
    var seen = new Set();
    return a.filter(item => {
        var k = key(item);
        return seen.has(k) ? false : seen.add(k);
    });
}

/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.postNearByFriend = function (req, res, next) {
    let latitude=req.body.lat,longitude=req.body.long;
    let data=[];let nearByLocations = null;let nearByUser =[];
    User.find( function (err, user) {
        for(let i=0;i<user.length;i++){
            data[i]=[user[i].lat,user[i].long,user[i].location];
        }
        const dataSet = Geo.createCompactSet(data);
        const geo = new Geo(dataSet, { sorted: true });
        nearByLocations= geo.nearBy(latitude, longitude, 5000);
        console.log(nearByLocations);
        if(!nearByLocations[0]){
            console.log("in else");
            res.send({success: false, message:"no near by user found"});
        } else {
            let b = uniqBy(nearByLocations, JSON.stringify);
            for(let i=0;i<b.length;i++){
                User.find({location:nearByLocations[i].i}, function (err, user) {
                    for(let j=0;j<user.length;j++){
                        nearByUser[j]={id:user[j]._id,firstName:user[j].firstName,location:user[j].location,picture:user[j].picture};
                    }
                    if(i === b.length-1){
                        res.send({success: true, data: nearByUser});
                    }

                })
            }
        }

    });

};