let Group = require('../models/group');
let User = require('../models/user');
const currentUser = require('../utils/currentUser');
const nodemailer = require('nodemailer');


exports.get = function get(req, res, next) {
    Group.find(function (err, group) {
        if (err) throw err;
        res.send({success: true, data: group});
    });
};
exports.post = function post(req, res, next) {
    console.log(req.body);

    const group = new Group(req.body);
    group.save(function (err) {
        if (err) {
            return res.send({success: false});
        }
        return res.status(201).send({success: true, data: group});
    });
};

exports.post = function post(req, res, next) {

    const group = new Group(req.body);
    group.save(function (err) {
        if (err) {
            return res.send({success: false});
        }
        return res.status(201).send({success: true, data: group});
    });
};

exports.addMember = function post(req, res, next) {
    let groupId = req.body.groupId;
    let userId = req.body.userId;

    console.log(groupId);

    Group.updateOne({_id: groupId}, {$addToSet: {'participants': userId}}, function (err, res) {
        if (err) {
            return res.send({success: false, message: err});
        }
        console.log("MEMBER ADDED");
    });
    return res.status(201).send({success: true, message: `user(${userId}) added to group (${groupId}) successfully`});

};

exports.sendReqJoinGroup = function post(req, res, next) {
    let groupId = req.body.groupId;
    let userId = req.body.userId;

    User.findOne({_id: userId}, function (err, user) {
        if (user) {
            res.send({success: true, msg: `request link has been sent to ${user.email}`})
            let smtpTransporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'chhavikohli.evontech@gmail.com',
                    pass: 'chhavi@123'
                }
            });
            let mailOptions = {
                to: user.email,
                from: 'chhavikohli.evontech@gmail.com',
                subject: `${currentUser.user.email} has requested you to join the group`,
                text: 'Hello,\n\n' +
                `Please click on the following link, or paste this into your browser to accept the request:\n\n
        http://${req.headers.host}/acceptRequest?users=${groupId}|${userId}`
            };

            smtpTransporter.sendMail(mailOptions, function (err) {
                console.log('mail sent');
                done(err, 'done');
            });

        }
    });
};
exports.acceptRequest = function post(req, res, next) {
    let id = req.query.users.split("|");


    Group.updateOne({_id: id[0]}, {$addToSet: {'participants': id[1]}}, function (err, res) {
        if (err) {
            return res.send({success: false, message: err});
        }
        console.log("MEMBER ADDED");
    });
    return res.status(201).send({success: true, message: `user(${id[0]}) added to group (${id[1]}) successfully`});

};
