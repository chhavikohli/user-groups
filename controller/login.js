const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const currentUser = require('../utils/currentUser');

/**
 * Password Checker
 * @param password
 * @returns {*}
 */
let passwordChecker = (password, userPassword) => {
    let hash = userPassword;
    return bcrypt.compareSync(password, hash);

};

/**
 * Post function to get user id and password
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
exports.post = function (req, res, next) {
    const payload = req.body;
    let email =req.body.email;
    if(!email){
        return res.send({success:false, data:'email empty'});
    }else{
        email= payload.email.toLowerCase();
    }
    User.findOne({email:email}, function (err, user) {

        if (user) {
            let status = passwordChecker(req.body.password, user.password);

            if (status) {
                jwt.sign({user}, 'secretkey', (err, token) => {

                    currentUser.user = {
                        id: user._id,
                        firstName: user.firstName,
                        lastName:user.lastName,
                        email: user.email,
                        picture: user.picture,
                        gender: user.gender,
                        dob: user.dob,
                        token: token
                    };
                    if (err) throw err;
                    return res.send({success: true, currentUser: currentUser.user});
                })
            } else {
                return res.send({success: false, data: "incorrect password"});
            }

        } else {
            return res.send({success: false, data: "incorrect email Id"});
        }

    });
};



