const stripe = require("stripe")("sk_test_BlKiIejTDeP1JGTRpgMHyELh");

/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.post = function (req, res, next) {
    let token = req.body.stripeToken; // Using Express
    let email = req.body.stripeEmail;

    stripe.customers.create({
        email: email
    }).then(function () {
        return stripe.charges.create({
            amount: 1600,
            currency: 'USD',
            source: token
        });
    }).catch((err) => {
            if (err) {
                console.log('message:', err.message);
            }
        }
    );
    return res.send({success: true});
};


