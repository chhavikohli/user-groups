const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const currentUser = require('../utils/currentUser');
const saltRounds = 10;

/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.get = function get(req, res, next) {
    let pageOptions = {
        page: req.query.offset || 0,
        limit: parseInt(req.query.limit) || 5
    };
    User.count({}, function (err, count) {
        let flag = count;
        flag = flag - (pageOptions.page * pageOptions.limit);
        if (flag <= 0) {
            return res.send({
                success: true,
                message: "you have reached the limit",

            });
        }
        User.find()
            .select('-password')
            .skip(pageOptions.page * pageOptions.limit)
            .limit(pageOptions.limit)
            .exec(function (err, doc) {
                if (err) {
                    res.status(500).json(err);
                    return;
                }
                res.status(200).json({
                    success: true,
                    "TotalCount": count,
                    "Array": doc
                });
            })
    })
};

/**
 * encrypts password
 * @param password
 * @returns {*}
 */
let encryption = (password) => {
    let hash = bcrypt.hashSync(password, saltRounds);
    return hash;
};
/**
 *
 * @param req
 * @param res
 * @param next
 */
exports.post = function (req, res, next) {
    let type = req.body.type;
    let errors = '';
    if (type === 'facebook') {
        req.check('email', 'Invalid email address').isEmail();
        req.check('facebookId', 'facebookId required').isLength({min: 1});
        errors = req.validationErrors();
        if (errors) {
            return res.send({success: false, message: errors});

        } else {
            User.findOne({email: req.body.email}, function (err, user) {
                jwt.sign({user}, 'secretkey', (err, token) => {
                    currentUser.user.token = token;
                    if (user) {
                        return res.send({success: true, data: token})
                    } else {
                        currentUser.user = {
                            id: req.body._id,
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            email: req.body.email.toLowerCase(),
                            registrationType: type,
                            facebookId: req.body.facebookId,
                            token: token
                        };
                        const users = new User(currentUser.user);
                        users.save(function (err, user) {
                            if (err) {
                                return res.send({success: false, message: err});
                            }
                            return res.send({success: true, currentUser: currentUser.user});
                        });
                    }
                });
            })
        }
    }
    else if (type === 'google') {
        req.check('email', 'Invalid email address').isEmail();
        req.check('googleId', 'googleId required').isLength({min: 1});
        errors = req.validationErrors();
        if (errors) {
            return res.send({success: false, message: errors});

        } else {
            User.findOne({email: req.body.email}, function (err, user) {
                jwt.sign({user}, 'secretkey', (err, token) => {

                    if (user) {
                        return res.send({success: true, data: token})
                    } else {
                        currentUser.user = {
                            id: req.body._id,
                            firstName: req.body.firstName,
                            lastName: req.body.lastName,
                            email: req.body.email.toLowerCase(),
                            registrationType: type,
                            googleId: req.body.googleId,
                            token: token
                        };
                        const users = new User(currentUser.user);
                        users.save(function (err, user) {
                            if (err) {
                                return res.send({success: false, message: err});
                            }
                            return res.send({success: true, currentUser: currentUser.user});
                        });
                    }
                });
            })
        }
    }
    else {
        req.check('email', 'Invalid email address').isEmail();
        req.check('password', 'Password length should be greater than 5').isLength({min: 5});

        errors = req.validationErrors();
        if (errors) {
            return res.send({success: false, message: errors});

        } else {

            let password = encryption(req.body.password);
            currentUser.user = {
                id: req.body._id,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email.toLowerCase(),
                gender: req.body.gender,
                registrationType: type,
                password: password,
                dob: req.body.dob,
                lat: req.body.lat,
                long: req.body.long,
                location: req.body.location
            };
            if (req.file) {
                currentUser.user.picture = req.file.filename;
            }

            const user = new User(currentUser.user);
            jwt.sign({user}, 'secretkey', (err, token) => {

                currentUser.user.token = token;
                user.save(function (err, user) {
                    if (err) {
                        return res.send({success: false, message: err});
                    }
                    return res.send({success: true, currentUser: currentUser.user});
                });
            })
        }
    }
};































