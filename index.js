let express = require('express');
let morgan = require('morgan');
let fs = require('fs');
const  path = require('path');
let signUp = require('./router/signup');
let login = require('./router/login');
let passwords = require('./router/passwords');
let friends = require('./router/friends');
let groups = require('./router/groups');
let users = require('./router/users');
let stripePayment = require('./router/payments');
const expressValidator = require('express-validator');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let app = express();
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

mongoose.connect('mongodb://localhost/new_project');

app.use(morgan('combined'));
app.use(expressValidator());

let accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});

app.use(morgan('combined', {stream: accessLogStream}));
app.use(express.static('views'));
app.use('/users', signUp);
app.use('/login',login);
app.use('/',passwords);
app.use('/',groups);
app.use('/',friends);
app.use('/',users);
app.use('/',stripePayment);
app.use('/static', express.static('views'));
let server = app.listen('3200', function () {
    console.log('application started at 3200');
});
