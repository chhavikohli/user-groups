let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// create a schema
let groupSchema = new Schema({
    groupName: { type: String, unique: true },
    participants: [],
    groupPicture: String,
    created_at: Date,
    updated_at: Date
});

// the schema is useless so far

// we need to create a model using it
let Group = mongoose.model('Group', groupSchema);

// make this available to our users in our Node applications
module.exports = Group;