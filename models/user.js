var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var userSchema = new Schema({
    firstName:{ type: String, required: true },
    lastName: String,
    email:{ type: String, unique: true,required:true },
    picture: String,
    password: String,
    dob: String,
    gender: String,
    lat:String,
    long:String,
    location:String,
    facebookId:String,
    googleId:String,
    created_at: Date,
    updated_at: Date,

});

// the schema is useless so far
// we need to create a model using it
let User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;