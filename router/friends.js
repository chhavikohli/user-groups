const express = require('express');
const upload = require('multer')({dest: './views/assets/'});
const router = express.Router();
const isAuthenticated = require('../utils/authenticate');
const friendsController = require('../controller/friends');

router.post('/searchFriends',isAuthenticated , upload.single('picture'), friendsController.postSearchFriend);
router.post('/getNearByFriends',isAuthenticated,  upload.single('picture'), friendsController.postNearByFriend);

module.exports = router;