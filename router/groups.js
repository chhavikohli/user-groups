const isAuthenticate = require('../utils/authenticate');
let express = require('express');
const upload  = require('multer')({ dest: './views/assets/' });
let router = express.Router();

let groupController = require('../controller/groups');
router.get('/getAllGroups',isAuthenticate, groupController.get);
router.post('/createGroup',isAuthenticate, upload.single('groupPicture'), groupController.post);
router.post('/addMember',isAuthenticate,upload.single('groupPicture'), groupController.addMember);
router.post('/sendReqJoinGroup',upload.single('groupPicture'), groupController.sendReqJoinGroup);
router.post('/acceptRequest',upload.single('groupPicture'), groupController.acceptRequest);

module.exports = router;