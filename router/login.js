const express = require('express');
const upload  = require('multer')({ dest: './views/assets/' });
const router = express.Router();

const loginController = require('../controller/login')

router.post('/',upload.single('picture'), loginController.post);

module.exports = router;