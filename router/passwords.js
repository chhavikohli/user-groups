const express = require('express');
const upload = require('multer')({dest: './views/assets/'});
const router = express.Router();
const isAuthenticated = require('../utils/authenticate');
const passwordController = require('../controller/passwords');

router.post('/resetPassword',isAuthenticated, upload.single('picture'), passwordController.post);
router.post('/forgotPassword',upload.single('picture'), passwordController.postForgetPassword);
router.post('/updatePassword/:id',upload.single('picture'), passwordController.postUpdatePassword);

module.exports = router;