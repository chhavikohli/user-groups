const express = require('express');
const upload = require('multer')({dest: './views/assets/'});
const router = express.Router();
const isAuthenticated = require('../utils/authenticate');
const paymentController = require('../controller/payments');

router.post('/stripePayment', upload.single('picture'), paymentController.post);


module.exports = router;