let express = require('express');
const multer = require('multer');
const path = require('path');
/*const upload = require('multer')({dest: './views/assets/'});*/
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './views/assets//')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
});

const upload = multer({ storage: storage });
let router = express.Router();

let usersController = require('../controller/signup')

router.get('/getAll', usersController.get);
router.post('/signUp',upload.single('picture'), usersController.post);

module.exports = router;