let express = require('express');
const upload  = require('multer')({ dest: './views/assets/' });
let router = express.Router();
const isAuthenticated = require('../utils/authenticate');
let usersController = require('../controller/users');

router.get('/getCurrentUser',isAuthenticated, usersController.get);
router.post('/editProfile',isAuthenticated,upload.single('picture'), usersController.post);

module.exports = router;