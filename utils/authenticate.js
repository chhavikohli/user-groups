const utils = require('./currentUser');

const isAuthenticate = (req, res, next) => {
    if (utils.user && utils.user.token === req.headers.authorization)
        return next();
    return res.send({success: false, message: 'Unauthorized user'})
};
module.exports = isAuthenticate;